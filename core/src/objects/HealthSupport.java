package objects;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.softnuke.biosleep.MyGame;

import screens.GameScreen;
import screens.MainMenuScreen;
import utils.AssetLord;
import utils.LevelGenerate;

/**
 * Created by Kush on 14-08-2016.
 */
public class HealthSupport {

	float width = 0.3f;
	float height = 0.3f;
	boolean visible = true;
	boolean DEAD = false;

	Body body;
	Fixture fixture;
	float timeLived = 0;
	static final float LIFE_DURATION = 3f;
	static final float IMPULSE_POWER = 0.015f;
	Sprite healthSprite;

	float impulseX, impulseY;
	World world;
	boolean interpolate = false;
	boolean CONSUMED = false;
	Vector2 position = new Vector2(0,0);
	float floatStrength = 0.1f;
	float time = 0;
	public Light light;

	public HealthSupport(World w){

		world = w;

		light = new Light(position, width*2, Light.RED_COLOR);
		//float ran = MathUtils.random(0, 0.1f);
		//width -= ran;
		//height -= ran;

		BodyDef bodyDef = new BodyDef();
		PolygonShape shape = new PolygonShape();
		FixtureDef fixtureDef = new FixtureDef();
		fixtureDef.shape = shape;

		//blood
		bodyDef.type = BodyDef.BodyType.KinematicBody;
		bodyDef.position.set(MyGame.bWIDTH/2, MyGame.bHEIGHT/2);
		bodyDef.allowSleep = true;
		bodyDef.fixedRotation = true;

		shape.setAsBox(width/2f, height/2f);
		fixtureDef.restitution = 0;
		fixtureDef.isSensor = true;
		fixtureDef.density = 0.2f;

		body = world.createBody(bodyDef);
		fixture = body.createFixture(fixtureDef);

		//body.setActive(false);
		//visible = false;
		//DEAD = true;
		shape.dispose();

		//Texture heart = new Texture("packit/heartSupport.png");
		//heart.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
		healthSprite = new Sprite(GameScreen.getInstance().getAssetLord().manager.get(AssetLord.game_atlas, TextureAtlas.class).findRegion("heartSupport"));
		healthSprite.setSize(width, GameScreen.getAssetRatio(healthSprite) * height);

	}

	public void render(SpriteBatch batch){
		if(!visible) return;

		healthSprite.setPosition(position.x - width/2, position.y - height/2);
		healthSprite.draw(batch, 0.7f);

	}

	public void update(float delta, float x, float y){
		if(DEAD) {
			if(body.isActive())
				cleanUp();

			return;
		}

		//don't show if consumed
		visible = !CONSUMED;

		if(visible)
			light.enable();
		else
			light.disable();

		time += delta;
		position.set(body.getPosition().x, body.getPosition().y);
		light.setPosition(position);

		//MyGame.sop(light.getPosition().x +",  " +light.getPosition().y);
		//timeLived += delta;

		//if(timeLived > LIFE_DURATION){
		//	setDead();
		//}

		//offset
		float tx = x - width*2;
		float ty = y + height*2;
/*
			float dx = tx - body.getPosition().x;
			float dy = ty - body.getPosition().y;

			float angle = MathUtils.atan2(dy, dx);

			double distance = Math.sqrt(((dx)*(dx)+(dy)*(dy)));

			float impx = (float) Math.cos(angle) * (float)distance * 0.002f * delta;
			float impy = (float) Math.sin(angle) * (float)distance * 0.002f * delta;

			body.applyLinearImpulse(impx, impy, body.getWorldCenter().x, body.getWorldCenter().y, true);
*/

		//TODO:
		//i think interpolation is broken
		float intx = Interpolation.bounceOut.apply(tx, position.x, delta*0.1f);
		float inty = Interpolation.bounceOut.apply(ty, position.y, delta*0.1f);

		body.setTransform(intx, inty + MathUtils.sin(time) * floatStrength ,0);

	}

	/** Don't do this in middle of world step */
	public void reset(float x, float y){
		visible = true;
		timeLived = 0;
		DEAD = CONSUMED = false;

		body.setActive(true);

		if(light != null)
			light.enable();

		impulseX = MathUtils.random(-IMPULSE_POWER, IMPULSE_POWER);
		impulseY = MathUtils.random(0, IMPULSE_POWER);

		body.setTransform(x, y, 0);

		body.applyLinearImpulse(impulseX, impulseY, 0, 0, true);
	}

	public boolean consume(){
		if(!CONSUMED)
		{//if not consumed already
			CONSUMED = true;
			if(light != null)
				light.disable();

			//show with 25% probability
			if(MathUtils.randomBoolean(0.25f))
				LevelGenerate.getInstance().visualfx.hurtDialogue();

			return true;
		}

		return false;
	}

	public void setDead(){
		//queue this for cleanup in next update
		visible = false;
		DEAD = true;

		if(light!=null)
			light.disable();
	}

	/** Don't do this in middle of world step */
	//will make this body inactive
	public void cleanUp(){
		body.setActive(false);
	}

	/**Add light to level */
	public void queueLight(){
		LevelGenerate.getInstance().pushLight(light);
	}
}
