package objects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.utils.Align;
import com.softnuke.biosleep.MyGame;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import screens.GameScreen;
import utils.AssetLord;

/**
 * Created by Kush on 17-08-2016.
 */
public class FX {

	static enum TYPE{
		DIALOGUE
	};
	public static final boolean BOKEH_EFFECT = true;

	BitmapFont font;
	Label textDialogue;
	static int DIALOGUE_TIME = 2; //sec time to show dialogue for
	float textTime = 0;
	boolean dialogueVisible = false;

	float time;
	float deltaParticles;

	World world;
	Stage stage;
	OrthographicCamera camera;
	OrthographicCamera cameraui;
	ParticleEffect bokehparticles;
	Vector3 textPos = new Vector3(0,0,0);

	public FX(World w, Stage s, OrthographicCamera cam, OrthographicCamera camui){
		world = w;
		stage = s;
		camera = cam;
		cameraui = camui;

		//font = GameScreen.getInstance().getAssetLord().manager.get(AssetLord.dialogue_font, BitmapFont.class);
		//font = new BitmapFont(Gdx.files.internal("fonts/ka.fnt"));
		//font = new BitmapFont(Gdx.files.internal("fonts/fipps.fnt"));
		font = new BitmapFont(Gdx.files.internal("fonts/joystx.fnt"));
		font.setUseIntegerPositions(true);
		font.getData().setScale(0.65f);


		init();
	}

	public void init(){

		Label.LabelStyle lStyle = new Label.LabelStyle();
		lStyle.font = font;
		lStyle.fontColor = Color.WHITE;
		textDialogue = new Label("Hey", lStyle);
		textDialogue.setAlignment(Align.center);
		textDialogue.setVisible(false);
		textDialogue.setPosition(MyGame.WIDTH/2, MyGame.HEIGHT/2);

		stage.addActor(textDialogue);


		bokehparticles = GameScreen._gameScreen.getAssetLord().manager.get(AssetLord.bokeh_particle, ParticleEffect.class);
		bokehparticles.setPosition(MyGame.bWIDTH/2,MyGame.bHEIGHT/2);
		bokehparticles.setEmittersCleanUpBlendFunction(false);

	}

	public void render(SpriteBatch batch){

	}

	public void renderParticles(SpriteBatch batch, float posx, float posy){
		if(BOKEH_EFFECT){
			bokehparticles.setPosition(posx, posy);
			bokehparticles.draw(batch);
		}

	}

	public void update(float delta){
		time += delta;

		if(dialogueVisible){
			textTime += delta;

			float px = Player.getInstance().getPosition().x + Player.getInstance().getWidth()*3 ;
			float py = Player.getInstance().getPosition().y + Player.getInstance().getHeight()*0.7f + textTime*0.2f;

			camera.project(textPos.set(px, py, 0));

			textDialogue.setPosition(textPos.x, textPos.y);

			//MyGame.sop("visible at "+px+","+py);

			if(textTime > DIALOGUE_TIME){
				textTime = 0;
				dialogueVisible = false;
			}
		}
		textDialogue.setVisible(dialogueVisible);

		if(GameScreen.RENDER_LIGHTS)
			bokehparticles.update(delta);
	}

	public void reset(){
		dialogueVisible = false;


	}

	public void showText(String text){
		textDialogue.setVisible(false);
		textDialogue.setText(text);
		dialogueVisible = true;

		MyGame.sop("Text Shown: "+text);
	}

	public void enemyKillDialogue() {
		//don't show if already visible currently
		if(dialogueVisible == true)
			return;

		int l = killDialogues.length-1;
		showText(killDialogues[MathUtils.random(0, l)]);
	}

	public void hurtDialogue(){
		//don't show if already visible currently
		if(dialogueVisible == true)
			return;

		int l = hurtDialogues.length-1;
		showText(hurtDialogues[MathUtils.random(0, l)]);
	}

	public void dispose(){
		reset();
	}

	public String hurtDialogues[] = {
			"Ouch", "Pain", "Me angry", "Damn", "Big mistake", "Come on"
	};

	public String killDialogues[] = {
		"Boo Yeah", "Dont mess with me", "Easy peezy", "AAhhhhh", "Hohoho", "Oh boy", "RATATATATATA"
	};
}
