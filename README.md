# ProfessorK
Platformer Game built in libgdx

Specializes in realistic physics using box2d. Collisions, recoil, projectiles,
particle effects, lights are simulated in real-time with great optimization. 
Won 2nd place out of 8000 entries in Hackerearth GSK hackathon.